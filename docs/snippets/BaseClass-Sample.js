
var Parent = function(value) {
  this.a = function(){return value};
};

var Child = function(value1, value2) {
  this.super(value1);
  this.b = function(){return value2};
};

Child.prototype = new Parent();
Child.prototype.super = Parent.prototype.constructor;
Child.prototype.constructor = Child;

console.log(new Parent("3").a());
var child = new Child("4", "5");
console.log(child.a());
console.log(child.b());


