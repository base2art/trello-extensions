describe("String", function() {


  it("Should Do Ends With Correctly", function() {
    expect(com.b2a.lang.String.endsWith("123456789", "789")).toBe(true);
    expect(com.b2a.lang.String.endsWith("123456789", "123456789")).toBe(true);
    expect(com.b2a.lang.String.endsWith("123456789", "1123456789")).toBe(false);
    expect(com.b2a.lang.String.endsWith("1234567889", "789")).toBe(false);
  });
  
  it("Should Do Starts With Correctly", function() {
    expect(com.b2a.lang.String.startsWith("123456789", "123")).toBe(true);
    expect(com.b2a.lang.String.startsWith("123456789", "123456789")).toBe(true);
    expect(com.b2a.lang.String.startsWith("123456789", "1234567899")).toBe(false);
    expect(com.b2a.lang.String.startsWith("12234567889", "123")).toBe(false);
  });
  
  
  it("Should splits With Correctly simple use case", function() {
    var splits = com.b2a.lang.String.split("hello world");
    expect(splits.length).toBe(2);
    expect(splits[0]).toBe("hello");
    expect(splits[1]).toBe("world");
  });
  
  it("Should splits With Correctly contiigous seps", function() {
    var splits = com.b2a.lang.String.split("hello  world");
    expect(splits.length).toBe(2);
    expect(splits[0]).toBe("hello");
    expect(splits[1]).toBe("world");
  });
  
  it("Should splits With Correctly alternate splitter", function() {
    var splits = com.b2a.lang.String.split("hello : world", ':');
    expect(splits.length).toBe(2);
    expect(splits[0]).toBe("hello ");
    expect(splits[1]).toBe(" world");
  });
  
  it("Should splits With Correctly multi-len sep", function() {
    var splits = com.b2a.lang.String.split("hello :: world", '::');
    expect(splits.length).toBe(2);
    expect(splits[0]).toBe("hello ");
    expect(splits[1]).toBe(" world");
  });
});
