describe("Lazy", function() {


  it("should be able to invoke method two Times and get 2 results no lazy", function() {
    var i = 0;
    var dateFunc = function(){return i++;};
    
    dateFunc();
    expect(i).toBe(1);
    
    dateFunc();
    expect(i).toBe(2);
  });

  it("should be able to invoke method two Times and only get fist result", function() {
    var i = 0;
    var date = new com.b2a.util.Lazy(function(){return i++;});
    
    date.value();
    expect(i).toBe(1);
    
    date.value();
    expect(i).toBe(1);
  });
});
