describe("Ref - CoreFuncs", function() {


  it("should coalesce undefined", function() {
    var b = {};
    expect(com.b2a.Ref.coalesce(b.a, null)).toBe(null);
    expect(com.b2a.Ref.coalesce(b.a, 3)).toBe(3);
  });
  
  it("should coalesce null", function() {
    
    expect(com.b2a.Ref.coalesce(null, null)).toBe(null);
    expect(com.b2a.Ref.coalesce(null, 2)).toBe(2);
  });
  
  it("should coalesce not coalesce value", function() {
    
    expect(com.b2a.Ref.coalesce("sdf", null)).toBe("sdf");
    expect(com.b2a.Ref.coalesce("sdf", 2)).toBe("sdf");
  });
  
  it("should coalesce not coalesce ref", function() {
    var a = {};
    expect(com.b2a.Ref.coalesce(a, null)).toBe(a);
    expect(com.b2a.Ref.coalesce(a, 2)).toBe(a);
  });
  
  it("should test isFunc Correctly", function() {
    var a = {};
    var b = function(){};
    expect(com.b2a.Ref.isFunc(a)).toBe(false);
    expect(com.b2a.Ref.isFunc(b)).toBe(true);
  });
});
