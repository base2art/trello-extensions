describe("WebConnections", function() {

  var System = require("b2a/System");
  var WebConnection = require("b2a/net/WebConnection");
    
  
  it("should Hash to QueryString correctly Single Param", function() {
    System.using(new WebConnection(), function(connection){
      expect(connection.toQueryString({"a": "b"})).toBe("a=b");
    });
  });
  it("should Hash to QueryString correctly Dual Param", function() {
    System.using(new WebConnection(), function(connection){
      expect(connection.toQueryString(
        {
          "doctor": "who", 
          "rose": "tyler"
        })).toBe("doctor=who&rose=tyler");
    });
  });
  
  it("should Hash to QueryString with spaces correctly", function() {
    System.using(new WebConnection(), function(connection){
      expect(connection.toQueryString({who: "bad wolf"})).toBe("who=bad%20wolf");
    });
  });
});
