describe("MXD-CssSelector", function() {

  var System = require("b2a/System");
  var CssSelector = require("b2a/mxd/CssSelector");
  
  
  it("Can split ok", function() {
    var items = new CssSelector("div.hello.world a#item span b#headerControl.header");
    expect(items.length).toBe(4);
    console.log(items);
    expect(items[0].name).toBe("div");
    expect(items[0].ids.length).toBe(0);
    expect(items[0].classes.length).toBe(2);
    expect(items[0].classes[0]).toBe("hello");
    expect(items[0].classes[1]).toBe("world");
    
    
    expect(items[1].name).toBe("a");
    expect(items[1].ids.length).toBe(1);
    expect(items[1].classes.length).toBe(0);
    expect(items[1].ids[0]).toBe("item");
    
    
    expect(items[2].name).toBe("span");
    expect(items[2].ids.length).toBe(0);
    expect(items[2].classes.length).toBe(0);
    
    
    expect(items[3].name).toBe("b");
    expect(items[3].ids.length).toBe(1);
    expect(items[3].classes.length).toBe(1);
    expect(items[3].ids[0]).toBe("headerControl");
    expect(items[3].classes[0]).toBe("header");
    
  });
});
