describe("MXD-Selector", function() {

  var System = require("b2a/System");
  var $ = require("b2a/mxd/Selector");
  var factory = require("b2a/mxd/DocumentFactory");
  
  var build = function(title, text){
    return "" + 
    "<html>" +
    "  <head>" +
    "    <title>" +
           title +
    "    </title>" +
    "  </head>" +
    "  <body>" + 
         text + 
    "  </body>" + 
    "</html>";
  }
  
  it("Find By Tag Correctly & id", function() {
    var text = build("", "<b id='item1'>1</b> <hr /> <b id='item2'>2</b>");
    var doc = factory.createHtml(text);
    expect($.doc(doc).tag("b").length).toBe(2);
    expect($.doc(doc).tag("b")[0].id).toBe("item1");
    expect($.doc(doc).tag("b")[1].id).toBe("item2");
    
    expect($.doc(doc).tag("hr").length).toBe(1);
  });
  
  it("Find By Tag And Inner Child Correctly", function() {
    var text = build("", "<div><b id='item1'>1</b></div> <hr /> <b id='item2'>2</b>");
    var doc = factory.createHtml(text);
    expect($.doc(doc).tag("div")[0].tag("b")[0].id).toBe("item1");
  });
  
  it("Find By Id", function() {
    var text = build("", "<div id='item1'><b id='item3'>1</b></div> <hr /> <b id='item2'>2</b>");
    var doc = factory.createHtml(text);
    var rez = $.doc(doc).id("item1");
    expect(rez.length).toBe(1);
    expect(rez[0].tag("b")[0].id).toBe("item3");
  });
  
  it("Find By Class", function() {
    var text = build("", "<div class='section' id='item1'><b>1</b></div> <hr /> <b id='item2' class='section'>2</b>");
    var doc = factory.createHtml(text);
    expect($.doc(doc).class("section").length).toBe(2);
    expect($.doc(doc).class("section")[0].id).toBe("item1");
    expect($.doc(doc).class("section")[1].id).toBe("item2");
  });
  
  // BETA
  
  it("Find By scope simple Element", function() {
    
    var text = build("", "<div class='section' id='item1'><b>1</b></div>" +
                         "<div class='section' id='item2'><a>1</a></div>" +
                         "<div class='section' id='item3'><a><b>1</b><b>1</b></a></div>" +
                         "<hr /> " +
                         "<div id='item2' class='section'>2</div>" +
                         "<div id='item3' class='section'><b>1</b></div>");
    var doc = factory.createHtml(text);
    expect($.doc(doc).scope("div").length).toBe(5);
  });
  
  it("Find By scope advanced", function() {
    
    var text = build("", "<div class='section' id='item1'><b>1</b></div>" +
                         "<div class='section' id='item2'><a>1</a></div>" +
                         "<div class='section' id='item3'><a><b>1</b><b>1</b></a></div>" +
                         "<hr /> " +
                         "<div id='item4' class='section'>2</div>" +
                         "<div><div id='item5' class='section'><b>1</b></div></div>");
    var doc = factory.createHtml(text);
    expect($.doc(doc).scope("div.section b").length).toBe(4);
    expect($.doc(doc).scope("div#item3 b").length).toBe(2);
    expect($.doc(doc).scope(".section b").length).toBe(4);
    expect($.doc(doc).scope("#item3 b").length).toBe(2);
    expect($.doc(doc).scope("div b").length).toBe(4);
  });
  
  it("Should get 'First' and 'last'", function() {
    var text = build("", "<div class='section' id='item1'><b>1</b></div>" +
                         "<hr /> " +
                         "<b id='item2' class='section'>2</b>" +
                         "<span id='item3' class='section'>3</span>");
    var doc = factory.createHtml(text);
    var coll = $.doc(doc).class("section");
    expect(coll.length).toBe(3);
    expect(coll.first.id).toBe("item1");
    expect(coll.last.id).toBe("item3");
    expect(coll.first$().id).toBe("item1");
    expect(coll.last$().id).toBe("item3");
    expect(coll.first$(function(x){return x.tagName == 'b'}).id).toBe("item2");
    expect(coll.last$(function(x){return x.tagName == 'b'}).id).toBe("item2");
    
  });
  
  it("Should not get 'First' and 'last' out of range", function() {
    
    var ElementCollection = require("b2a/mxd/ElementCollection");
    var coll = new ElementCollection([]);
    expect(coll.length).toBe(0);
    expect(coll.first).toBeNull();
    expect(coll.last).toBeNull();
    expect(coll.first$()).toBeNull();
    expect(coll.last$()).toBeNull();
    expect(coll.first$(function(x){return x.tagName == 'b'})).toBeNull();
    expect(coll.last$(function(x){return x.tagName == 'b'})).toBeNull();
  });
  
  it("Should not be able to add items", function() {
    
    var ElementCollection = require("b2a/mxd/ElementCollection");
    var coll = new ElementCollection([]);
    expect(coll.length).toBe(0);
    coll.push("sdf");
    expect(coll.length).toBe(0);
    coll[0] = "sdf";
    expect(coll.length).toBe(0);
  });
  
});
