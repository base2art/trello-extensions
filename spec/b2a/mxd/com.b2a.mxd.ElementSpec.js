describe("MXD-Element", function() {

  var System = require("b2a/System");
  var $ = require("b2a/mxd/Selector");
  var factory = require("b2a/mxd/DocumentFactory");
  
  var build = function(title, text){
    return "" + 
    "<html>" +
    "  <head>" +
    "    <title>" +
           title +
    "    </title>" +
    "  </head>" +
    "  <body>" + 
         text + 
    "  </body>" + 
    "</html>";
  }
  
  
  it("should get id", function() {
    var text = build("", "<b id='item1'>1</b> <hr /> <b id='item2'>2</b>");
    var doc = factory.createHtml(text);
   
    expect($.doc(doc).tag("b")[0].id).toBe("item1");
    expect($.doc(doc).tag("b")[1].id).toBe("item2");
    
    expect($.doc(doc).tag("hr")[0].id).toBe("");
  });
  
  
  it("should get textContent", function() {
    var text = build("", "<div id='item1'><b>1</b></div> <hr /> <b id='item2'>2</b>");
    var doc = factory.createHtml(text);
    expect($.doc(doc).tag("b")[0].textContent).toBe("1");
  });
  
  it("should get tagName", function() {
    var text = build("", "<div class='section'><b>1</b></div> <hr /> <b class='section'>2</b>");
    var doc = factory.createHtml(text);
    var sections = $.doc(doc).class("section");
    expect(sections.length).toBe(2);
    expect(sections[0].tagName).toBe("div");
    expect(sections[1].tagName).toBe("b");
  });
  
  
  it("Element.cssClass & Element.cssClasses", function() {
    var text = build("", "<div class='section header   item-1'><b>1</b></div> <hr /> <b class='section'>2</b>");
    var doc = factory.createHtml(text);
    expect($.doc(doc).class("section").length).toBe(2);
    expect($.doc(doc).class("section")[0].cssClasses.length).toBe(3);
    expect($.doc(doc).class("section")[0].cssClasses[0]).toBe("section");
    expect($.doc(doc).class("section")[0].cssClasses[1]).toBe("header");
    expect($.doc(doc).class("section")[0].cssClasses[2]).toBe("item-1");
  });
  
  
  it("should be able to add element and attributes", function() {
    var text = build("", "<div class='section'><b>1</b></div>");
    var doc = factory.createHtml(text);
    
    var coll = $.doc(doc).class("section");
    expect(coll.length).toBe(1);
    var parent = coll[0];
    parent.addElement("a", {"class": "header"});
    expect(parent.unwrap().innerHTML).toBe('<b>1</b><a class="header"></a>');
  });
  
  
  it("should be able to add element to parent", function() {
    var text = build("", "<div class='section'><b>1</b></div>");
    var doc = factory.createHtml(text);
    
    var coll = $.doc(doc).class("section");
    expect(coll.length).toBe(1);
    var parent = coll[0];
    parent.addElement("a", {"class": "header"})
          .addText("hello")
          .parent()
          .parent()
          .addElement("a", {"class": "middle"})
          .addText("world")
          .parent()
          .parent()
          .addElement("a", {"class": "footer"})
          .addText("!");
    expect(parent.unwrap().innerHTML).toBe('<b>1</b><a class="header">hello</a><a class="middle">world</a><a class="footer">!</a>');
  });
  
  
  it("should be able to add element after", function() {
    var text = build("", "<div class='section'><b>1</b></div>");
    var doc = factory.createHtml(text);
    
    var coll = $.doc(doc).class("section");
    expect(coll.length).toBe(1);
    var parent = coll[0];
    parent.tag("b")[0]
      .addElementAfter("a", {"class": "header"})
        .addText("hello")
        .parent()
      .addElementAfter("a", {"class": "middle"})
        .addText("world")
        .parent()
      .addElementAfter("a", {"class": "footer"})
        .addText("!");
    expect(parent.unwrap().innerHTML).toBe('<b>1</b><a class="header">hello</a><a class="middle">world</a><a class="footer">!</a>');
  });
  
  it("should be able to add element before", function() {
    var text = build("", "<div class='section'><b>1</b></div>");
    var doc = factory.createHtml(text);
    
    var coll = $.doc(doc).class("section");
    expect(coll.length).toBe(1);
    var parent = coll[0];
    parent.tag("b")[0]
      .addElementBefore("a", {"class": "header"})
        .addText("hello")
        .parent()
      .addElementBefore("a", {"class": "middle"})
        .addText("world")
        .parent()
      .addElementBefore("a", {"class": "footer"})
        .addText("!");
    expect(parent.unwrap().innerHTML).toBe('<a class="footer">!</a><a class="middle">world</a><a class="header">hello</a><b>1</b>');
  });
  
  it("should be able to set Attribute", function() {
    var text = build("", "<div class='section'></div>");
    var doc = factory.createHtml(text);
    
    var coll = $.doc(doc).class("section");
    expect(coll.length).toBe(1);
    var parent = coll[0];
    parent.addElement("a", {"class": "header"})
        .setAttr("href", "#")
        .addText("hello");
    expect(parent.unwrap().innerHTML).toBe('<a class="header" href="#">hello</a>');
  });
  
  it("should be able to get Attribute", function() {
    var text = build("", "<div class='section'></div>");
    var doc = factory.createHtml(text);
    
    var coll = $.doc(doc).class("section");
    expect(coll.length).toBe(1);
    var parent = coll[0];
    var x = parent.addElement("a", {"class": "header"})
        .setAttr("href", "#")
        .addText("hello")
        .parent();
    expect(parent.unwrap().innerHTML).toBe('<a class="header" href="#">hello</a>');
    expect(x.attr("href")).toBe("#");
    expect(x.attr("class")).toBe("header");
  });
  
  it("should be able to get Children Elements", function() {
    var text = build("", "<div class='section'><b></b><i></i>Test &#160;</div>");
    var doc = factory.createHtml(text);
    
    var coll = $.doc(doc).class("section")[0];
    expect(coll.elementChildren.length).toBe(2);
  });
  
  it("should be able to remove Children Elements", function() {
    var text = build("", "<div class='section'><b></b><i></i>Test &#160;</div>");
    var doc = factory.createHtml(text);
    
    var coll = $.doc(doc).class("section")[0];
    coll.elementChildren.first.remove();
    expect(coll.elementChildren.length).toBe(1);
    expect(coll.elementChildren.first.tagName).toBe("i");
  });
  
  it("should be able to get Children Test", function() {
    var text = build("", "<div class='section'><b></b><i></i>Test &#38;</div>");
    var doc = factory.createHtml(text);
    
    var coll = $.doc(doc).class("section")[0];
    expect(coll.textChildren.length).toBe(1);
    expect(coll.textChildren.first.value).toBe("Test &");
  });
  
  it("should be able to add/remove Class", function() {
    // <i class='italClass'></i>
    var text = build("", "<div class='section'><b>a</b></div>");
    var doc = factory.createHtml(text);
    
    var coll = $.doc(doc).class("section")[0];
    var b = coll.tag("b")[0];
    b.addClass("class1");
    expect(coll.unwrap().innerHTML).toBe('<b class="class1">a</b>');
    b.addClass("class2");
    expect(coll.unwrap().innerHTML).toBe('<b class="class1 class2">a</b>');
    b.addClass("class3");
    expect(coll.unwrap().innerHTML).toBe('<b class="class1 class2 class3">a</b>');
    b.removeClass("class1");
    expect(coll.unwrap().innerHTML).toBe('<b class="class2 class3">a</b>');
  });
});

