describe("System - CoreFuncs", function() {


  it("should be able to dispose", function() {
    var b = 11;
    com.b2a.System.using({dispose:function(){b++;}, a:1}, function(x){
      
      expect(x.a).toBe(1);
      
    });
    
    expect(b).toBe(12);
  });
  
  it("should be able to dispose when error", function() {
    var b = 11;
    
    expect(function() {
      com.b2a.System.using({dispose:function(){b++;}, a:1}, function(x){
        throw "error";
      });
    }).toThrow();
    
    expect(b).toBe(12);
  });
  
  it("should be able to require", function() {
    expect(sys).toBeUndefined();
    var sys = require("b2a/System");
    expect(sys).not.toBeNull();
    expect(sys).not.toBeUndefined();
  });

  it("should be able to extend object with function already exists", function() {
    expect(com.b2a.hello).toBeUndefined();
    var result = com.b2a.System.extend(["com", "b2a", "hello"], function(){});
    expect(com.b2a.hello).not.toBeNull();
    expect(com.b2a.hello).not.toBeUndefined();
    expect(result).not.toBeUndefined();
    expect(result).not.toBeNull();
  });
  
  it("should be able to extend object with function new ns", function() {
    expect(com.scottyoungblut).toBeUndefined();
    var result = com.b2a.System.extend(["com", "scottyoungblut", "hello"], function(){});
    expect(result).not.toBeNull();
    expect(result).not.toBeUndefined();
    expect(result).not.toBeNull();
    expect(result).not.toBeUndefined();
  });
  
  it("should be able to extend object with object new ns", function() {
    expect(com.b2a.colors).toBeUndefined();
    var result = com.b2a.System.extend(["com", "b2a", "colors"], {
      color: "black"
    });
    
    expect(com.b2a.colors.color).toBe("black");
    expect(result.color).toBe("black");
  });
  
  it("should be able to extend object with object old", function() {
    expect(com.b2a.colors2).toBeUndefined();
    var result = com.b2a.System.extend(["com", "b2a", "colors2"], {
      color: "black"
    });
    
    expect(result.color).toBe("black");
    
    result = com.b2a.System.extend(["com", "b2a", "colors2"], {
      newcolor: "gold"
      
    });
    
    expect(com.b2a.colors2.color).toBe("black");
    expect(com.b2a.colors2.newcolor).toBe("gold");
    expect(result.color).toBe("black");
    expect(result.newcolor).toBe("gold");
  });
});
