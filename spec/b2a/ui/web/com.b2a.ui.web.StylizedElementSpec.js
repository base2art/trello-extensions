describe("MXD-Element", function() {

  var System = require("b2a/System");
  var $ = require("b2a/mxd/Selector");
  var factory = require("b2a/mxd/DocumentFactory");
  var $ui = require("b2a/ui/web/Style");
  
  var build = function(title, text){
    return "" + 
    "<html>" +
    "  <head>" +
    "    <title>" +
           title +
    "    </title>" +
    "  </head>" +
    "  <body>" + 
         text + 
    "  </body>" + 
    "</html>";
  }
  
  
  it("should get styles", function() {
    var text = build("", "<b id='item1'>1</b>");
    var doc = factory.createHtml(text);
   
    var b1 = $.doc(doc).tag("b")[0];
    expect($ui(b1).style.display).toBe("");
    expect($ui($.doc().tag("body")[0]).computedStyle.display).toBe("block");
  });
});
