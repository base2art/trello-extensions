

define("b2a/mxd/Element",
      ["b2a/System", "b2a/lang/String", "b2a/mxd/ScopingNode", "b2a/mxd/Text"],
  function(System, String, ScopingNode, Text) {
    var klazz = System.extend("com.b2a.mxd.Element", function(nativeElement){
      this.superScopingNode(nativeElement);
      this.__native = nativeElement;
      //~ this.id = nativeElement.id;
      
      Object.defineProperty(this, "id",  
                { get : function(){ return this.__native.id; }});
      
      Object.defineProperty(this, "textContent",  
                { get : function(){ return this.__native.textContent; }});
                
      Object.defineProperty(this, "tagName",
                { get : function(){ return this.__native.tagName.toLowerCase(); }});
                
      Object.defineProperty(this, "cssClass",
                { get : function(){ return this.__native.getAttribute("class"); }});
                
      Object.defineProperty(this, "cssClasses", 
                { get : function(){ return this.cssClass == null ? [] : String.split(this.cssClass); } });
                
      Object.defineProperty(this, "elementChildren",
                { get : function(){ return this._toCollForEl(this._filterNodeListBy(this.__native.childNodes, this.__native.ELEMENT_NODE)); }});
      
      Object.defineProperty(this, "textChildren",
                { get : function(){ return this._toCollForText(this, this._filterNodeListBy(this.__native.childNodes, this.__native.TEXT_NODE)); }});
      
      this.addClass = function(className) {
        
        var classes = this.cssClasses;
        if (classes.some(function(x) { return x == className; }))
        {
          return this;
        }
        
        classes.push(className);
        this.__native.setAttribute("class", classes.join(" "));
        return this;
      };
      
      this.removeClass = function(className) {
        
        var classes = this.cssClasses;
        if (classes.some(function(x) { return x == className; }))
        {
          var index = classes.indexOf(className);
          classes.splice(index, 1);
          this.__native.setAttribute("class", classes.join(" "));
          return this;
        }
        
        return this;
      };
      
      this.addElement = function(name, attrs) {
        
        var el = this.__createEl(name, attrs);
        this.__native.appendChild(el.unwrap());
        return el;
      };
      
      this.addElementAfter = function(name, attrs) {
        
        var el = this.__createEl(name, attrs);
        this.__native.parentNode.insertBefore(el.unwrap(), this.__native.nextSibling);
        return el;
      };
      
      this.addElementBefore = function(name, attrs) {
        
        var el = this.__createEl(name, attrs);
        this.__native.parentNode.insertBefore(el.unwrap(), this.__native);
        return el;
      };
      
      this.addText = function(text) {
        var node = this.__native.ownerDocument.createTextNode(text);
        this.__native.appendChild(node);
        //~ if (Text == null) {
          //~ Text = require("b2a/mxd/Text");
        //~ }
        
        return new Text(this, node);
      };
      
      this.remove = function() {
        
        var parentNode = this.__native.parentNode;
        if (parentNode != null)
        {
          parentNode.removeChild(this.__native);
        }
      }
       
      this.parent = function() {
        return new com.b2a.mxd.Element(this.__native.parentNode);
      };
      
      this.setAttr = function(name, value) {
        this.__native.setAttribute(name, value);
        return this;
      };
      
      this.attr = function(name) {
        return this.__native.getAttribute(name);
      };
      
      this.__createEl = function(name, attrs) {
        var el = this.__native.ownerDocument.createElement(name);
        for(var key in attrs) {
          el.setAttribute(key, attrs[key]);
        }
        
        return new com.b2a.mxd.Element(el);
      };
      
      this.toString = function() {
          return "[object Element]";
      }
    });
    
    klazz.prototype = new ScopingNode();
    klazz.prototype.superScopingNode = ScopingNode.prototype.constructor;
    klazz.prototype.constructor = klazz;
    
    return klazz;
  }
);
