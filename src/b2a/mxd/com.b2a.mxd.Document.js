

define("b2a/mxd/Document",
      ["b2a/System", "b2a/mxd/ScopingNode"],
  function(System, ScopingNode) {
    var klazz = System.extend("com.b2a.mxd.Document", function(nativeDocument){
      this.superScopingNode(nativeDocument);
    });
    
    klazz.prototype = new ScopingNode();
    klazz.prototype.superScopingNode = ScopingNode.prototype.constructor;
    klazz.prototype.constructor = klazz;
    
    return klazz;
  }
);
