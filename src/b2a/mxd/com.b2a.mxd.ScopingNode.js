


define("b2a/mxd/ScopingNode",
      ["b2a/System", "b2a/lang/String", "b2a/mxd/Node", "b2a/mxd/CssSelector"],
  function(System, String, Node, CssSelector) {
    var klazz = System.extend("com.b2a.mxd.ScopingNode", function(native){
      this.superNode(native);
      this.native = native;
      
      
      // returns list of Dom Elements.
      // tagName: string
      this.tag = function (tagName) {
        var nl = this.native.getElementsByTagName(tagName);
        return this._toCollForEl(nl);
      };
      
      // returns list of Dom Elements.
      // tagName: string
      this.class = function (cssClass) {
        var nl = this.native.getElementsByClassName(cssClass);
        return this._toCollForEl(nl);
      };
      
      // returns list of Dom Elements.
      // tagName: string
      this.id = function (elementId) {
        var nl = this.native.getElementById(elementId);
        return this._toCollForEl([nl]);
      };
      
      // returns list of Dom Elements.
      // tagName: string
      this.scope = function (query) {
        
        var selector = new CssSelector(query).reverse();
        var item;
        var currentNodes = [this];
        while ((item = selector.pop()) != null) {
          var newCurrentNodes = [];
          var currentNode = null;
          
          while ((currentNode = currentNodes.pop()) != null) {
            var temp = null;
            if (item.name != null) {
              temp = currentNode.tag(item.name);
            }
            else if (item.ids != null && item.ids.length > 0) {
              temp = currentNode.id(item.ids[0]);
            }
            else if (item.classes != null && item.classes.length > 0) {
              temp = currentNode.class(item.classes[0]);
            } else {
              throw "OutOfRange"
            }
            
            if (temp != null){
              if (item.ids != null && item.ids.length > 0) {
                var id = null
                while ((id = item.ids.pop()) != null) {
                  temp = temp.filter(function(x) { return x.id == id; });
                }
              }
              
              if (item.classes != null && item.classes.length > 0) {
                var klazz = null
                while ((klazz = item.classes.pop()) != null) {
                  temp = temp.filter(function(x) { return x.cssClasses.some(function(y) { return y  == klazz; }); });
                }
              }
            }
            
            temp.forEach(function(y){
              
              if (!newCurrentNodes.some(function(x) { return x.native == y.native; })) {
                newCurrentNodes.push(y);
              }
            });
          }
          
          currentNodes = newCurrentNodes;
        }
        
        return this._toCollForEl(currentNodes);
      };
      
    });
    
    klazz.prototype = new Node();
    klazz.prototype.superNode = Node.prototype.constructor;
    klazz.prototype.constructor = klazz;
    
    return klazz;
  }
);

