

define("b2a/mxd/CssSelector",
      ["b2a/System", "b2a/lang/String"],
  function(System, String) {
    
    var klass = function (query) {
      var parts = String.split(query);
      var i = 0;
      
      var rezults = []
      for (i = 0; i < parts.length; i++) {
        var current = parts[i];
        
        var j = 0;
        var buffer = [];
        var ids = [];
        var classes = [];
        var name = null;
        
        for(j = current.length - 1; j >= 0; j--) {
          
          var letter = current[j];
          if ("." == letter || "#" == letter) {
            
            (letter == "#" ? ids : classes).push(buffer.reverse().join(""));
            buffer = []
            
          } else {
            buffer.push(current[j]);
          }
        }
          
        if (buffer.length > 0) {
          name = buffer.reverse().join("");
        }
        
        this.push({
          "ids" : ids.reverse(),
          "classes" : classes.reverse(),
          "name": name
        });
      }
      
      //~ Object.freeze(this);
    };
    
    klass.prototype = [];
    klass.prototype.constructor = klass;
    return System.extend("com.b2a.mxd.CssSelector", klass);
  }
);
