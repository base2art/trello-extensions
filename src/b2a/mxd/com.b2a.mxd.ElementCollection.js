

define("b2a/mxd/ElementCollection",
      ["b2a/System", "b2a/Ref"],
  function(System, Ref) {
    var CollectionClass = function(items) {
      
      items = items == null ? [] : items;
      //~ System.extendAsArray(this);
      var i = 0;
      for (i=0; i<items.length; i++) {
        this.push(items[i]);
      }
      
      Object.defineProperty(this, "first", {get : function(){ return Ref.coalesce(this[0], null); }});
      Object.defineProperty(this, "last", {get : function(){ return Ref.coalesce(this[this.length - 1], null); }});
      
      this.first$ = function(func) {
        
        if (func == null) {
          var f = this.first;
          return f;
        }
        
        return Ref.coalesce(this.filter(func)[0], null);
      };
      
      this.last$ = function(func) {
        
        if (func == null) {
          var f = this.last;
          return f;
        }
        
        return Ref.coalesce(this.reverse().filter(func)[0], null);
      };
      
      
      Object.freeze(this);
    };
    
    CollectionClass.prototype = new Array();
    CollectionClass.prototype.constructor = CollectionClass;
    return System.extend("com.b2a.mxd.ElementCollection", CollectionClass);
  }
);
