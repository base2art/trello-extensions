
lazy_import("b2a/mxd/Element");
lazy_import("b2a/mxd/Text");

define("b2a/mxd/Node",
      ["b2a/System", "b2a/mxd/Element", "b2a/mxd/Text", "b2a/mxd/ElementCollection"],
  function(System, Element, Text, ElementCollection) {
    return System.extend("com.b2a.mxd.Node", function(native){
      this.native = native;
      
      
      // returns the underlying Node.
      this.unwrap = function (tagName) {
        return this.native;
      };
      
      
      // returns list of Dom Elements.
      // nodeList: the items to conver
      this._toCollForEl = function(nodeList) {
        var nodeArray = [];
        for (var i = 0; i < nodeList.length; ++i) {
          nodeArray[i] = new Element(nodeList[i]);
        }
        
        return new ElementCollection(nodeArray);
      };
      
      // returns list of Dom TextNodes.
      // nodeList: the items to conver
      this._toCollForText = function(parent, nodeList) {
        var nodeArray = [];
        for (var i = 0; i < nodeList.length; ++i) {
          nodeArray[i] = new Text(parent, nodeList[i]);
        }
        
        return new ElementCollection(nodeArray);
      };
      
      this._filterNodeListBy = function(nodeList, nodeType) {
        var nodeArray = [];
        
        for (var i = 0; i < nodeList.length; ++i) {
          var current = nodeList[i];
          if (nodeType == current.nodeType) {
            nodeArray.push(current);
          }
        }
      
        return nodeArray;
      };
    });
  }
);
