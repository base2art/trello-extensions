

define("b2a/mxd/Text",
      ["b2a/System", "b2a/mxd/Node"],
  function(System, Node) {
    var klazz = System.extend("com.b2a.mxd.Text", function(parentNode, nativeText){
      this.super(nativeText);
      this.__nativeText = nativeText;
      
      Object.defineProperty(this, "value",
                { 
                  get : function(){ return this.__nativeText.data; },
                  set : function(val) { return this.__nativeText.data = val; }
                });
      
      this.parent = function() {
        return parentNode;
      };
    });
    
    klazz.prototype = new Node();
    klazz.prototype.super = Node.prototype.constructor;
    klazz.prototype.constructor = klazz;
    
    return klazz;
  }
);
