

define("b2a/mxd/DocumentFactory", ["b2a/System"],
  function(System) {
    return System.extend("com.b2a.mxd.DocumentFactory", {
      
      createHtml: function(text) {
        var newDom = document.implementation.createHTMLDocument("example");
        newDom.documentElement.innerHTML = text;
        return newDom;
      }
    });
  }
);
