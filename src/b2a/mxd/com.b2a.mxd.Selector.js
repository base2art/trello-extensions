


define("b2a/mxd/Selector",
      ["b2a/System", "b2a/mxd/Document", "b2a/mxd/Element"],
  function(System, Document, Element) {
    return System.extend("com.b2a.mxd.Selector", {
      
      doc: function(documentElement) {
        return new Document(documentElement == null ? document : documentElement);
      },
      
      tag: function (tagName) {
        return this.doc().tag(tagName);
      },
      
      // returns list of Dom Elements.
      // tagName: string
      class: function (cssClass) {
        return this.doc().class(cssClass);
      },
      
      // returns list of Dom Elements.
      // tagName: string
      id: function (elementId) {
        return this.doc().id(elementId);
      },
      
      // returns list of Dom Elements.
      // tagName: string
      scope: function (query) {
        return this.doc().scope(query);
      }
    });
  }
);
