
define("b2a/Ref",
      ["b2a/System"],
  function(System) {
    return System.extend("com.b2a.Ref", {
      
      // objToCheck: is obj or null
      isFunc : function (objToCheck) {
        return System.isFunc(objToCheck);
      },
      
      coalesce : function (item1, defaultValue) {
        if (item1 != null) {
          return item1;
        }
        
        return defaultValue;
      }
    });
  }
);
