
define("b2a/util/Lazy", ["b2a/System"],
  function(System) {
    return System.extend("com.b2a.util.Lazy", function(func) {
      this.__func = func;
      this.__hasCalled = false;
      this.__result = null;

      this.value = function() {
        if (!this.__hasCalled) {
          this.__result = this.__func();
          this.__hasCalled = true;
        }

        return this.__result;
      };
    });
  }
);
