



$b2a_jscorlib_io_DiskRequire = function(globalObj) {
  var clk = "$CLASS_LOADER$";
  globalObj[clk] = {
    doc : function() {return document;},
    global : function() {return globalObj;},
    
    modules : {
    },
    
  };
  var classloader = globalObj[clk];
  
  globalObj["define"] = function(name, reqs, func){
    var i = 0;
    var list = []
    var mc = classloader.modules;
    for (i=0; i< reqs.length; i++)
    {
      var dep = reqs[i];
      var item = mc[dep];
      if (item == null) {
        var FakeClass = function() {
          var argsArray = Array.prototype.slice.call(arguments);
          this.superProxyCtor.apply(this, argsArray);
        };
        
        mc[dep]  = {
          "name" : dep,
          "requirements" : [],
          "init" : null,
          "value": null,
          "instances" : [ FakeClass ]
        };
        
        list.push(FakeClass);
      } else if (item.value == null) {
        
        var FakeClass = function() {
          var argsArray = Array.prototype.slice.call(arguments);
          this.superProxyCtor.apply(this, argsArray);
        };
        
        item.instances.push(FakeClass);
        list.push(FakeClass);
      } else {
        list.push(item.value);
      }
    }
    
    var oldDefine = mc[name];
    if (oldDefine == null)
    {
      mc[name] = {
        "name" : name,
        "requirements" : reqs,
        "init" : func,
        "value": null,
        "instances" : []
      };
      oldDefine = mc[name];
    } else {
      oldDefine.requirements = reqs;
      oldDefine.init = func;
    }
    
    var value = func.apply(func, list);
    oldDefine.value = value;
    if (oldDefine.instances.length > 0) {
      
      oldDefine.instances.forEach(function(FakeClass) {
        var newValue = new value();
        FakeClass.prototype = newValue;
        FakeClass.prototype.superProxyCtor = value.prototype.constructor;
        FakeClass.prototype.constructor = FakeClass;
      });
    }
    oldDefine.instances = [];
  };
  
  globalObj["lazy_import"] = function(name) { };  
  
  globalObj["require"] = function(reqs, func){
    
    var mc = classloader.modules;
    
    // HACK TEST FOR STRING
    if (func == null && reqs["split"] != null)
    {
      return mc[reqs] == null ? null : mc[reqs].value;
    }
    
    var i = 0;
    var list = []
    for (i=0; i< reqs.length; i++)
    {
      var dep = mc[reqs[i]];
      list.push(dep == null ? null : dep.value);
    }
    
    if (func == null)
    {
      return list;
    }
    
    return func.apply(func, list);
  };
  
  return classloader;
};

if (window["define"] == null) {
  if (window["com"] == null)
  {
    window["com"] = {
      b2a : {
        System : {
          __classloader : $b2a_jscorlib_io_DiskRequire(window),
          classloader : function() {
            return this.__classloader;
          }
        }
      }
    }
  }
} 

