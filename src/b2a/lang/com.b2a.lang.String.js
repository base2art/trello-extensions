
define("b2a/lang/String", ["b2a/System"],
  function(System) {
    
    var StringKlazz = System.extend("com.b2a.lang.String", function(nativeValue) {
      
    });
    
    
    StringKlazz["endsWith"] = function(value, test) {
      return test == value.substring(value.length - test.length, value.length);
    };
    
    StringKlazz["startsWith"] = function(value, test) {
      return test == value.substring(0, test.length);
    };
    
    StringKlazz["split"] = function(value, sep) {
      if (sep == null) {
        sep = " ";
      }
      
      return value.split(sep).filter(function(x){return x != "";});
    };
    
    return StringKlazz;
  }
);
