

// NEEDS TESTS
define("b2a/net/WebConnection", ["b2a/System"],
  function(System) {
    return System.extend("com.b2a.net.WebConnection", function() {
      
      // returns new Config;
      this.config = function(){
        var klazz = function() {
          this.method = "GET";
          this.parameters = {};
          this.success = function(x,y){};
          this.done = function(x,y){};
          this.fail = function(x,y){};
          this.context = {};
          this.withSuccess = function(successFunc) {
            var inst = new klazz();
            inst.method = this.method;
            inst.parameters = {};
            
            inst.success = successFunc;
            inst.done = this.done;
            inst.fail = this.fail;
            inst.context = this.context;
            
            for (var key in this.parameters) {
              inst.parameters[key] = this.parameters[key];
            }
            
            return inst;
          }
        };
        
        return new klazz;
      };
      
      // TREAT AS PRIVATE
      this.isAlphaNumeric = function(ch) {
        
        //  0                 9
        if (48 <= ch && ch <= 57) {
          return true;
        }
        
        //  A                 Z
        if (65 <= ch && ch <= 90) {
          return true;
        }
        
        //  a                 z
        if (97 <= ch && ch <= 122) {
          return true;
        }
        
        return false;
      };
      
      // returns string
      // value: string
      this.urlEncode = function(value) {
        var rez = [];
        var i=0;
        for (i=0; i< value.length; i++) {
          
          var code = value.charCodeAt(i);
          if (this.isAlphaNumeric(code)) {
            rez.push(value.charAt(i));
          }
          else {
            rez.push("%" + code.toString(16));
          }
        }
        
        return rez.join("");
      }
      
      // returns string
      // parameters: hash
      this.toQueryString = function(parameters) {
        var rez = [];
        for (key in parameters)
        {
          var kvp = []
          kvp.push(this.urlEncode(key));
          kvp.push(this.urlEncode(parameters[key]));
          
          rez.push(kvp.join("="));
        }
        
        return rez.join("&");
      };
      
      // PRIVATE
      // returns nothing
      // url: a string uri
      // config: object of(Config)
      this.__requestAsyncInternal = function(url, config, readyStateChangeHandler) {
        var xmlHttp = new XMLHttpRequest();
        var parameters = this.toQueryString(config.parameters);
        xmlHttp.open(config.method, url, true);

        xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        //~ if (config.method != "GET") {
          //~ xmlHttp.setRequestHeader("Content-length", parameters.length);
        //~ }
        //~ 
        //~ try {
          //~ xmlHttp.setRequestHeader("Connection", "close");
        //~ } catch (err) {
        //~ }

        xmlHttp.onreadystatechange = function() { readyStateChangeHandler(xmlHttp);};

        if (config.method == "GET") {
          xmlHttp.send();
        } else {
          xmlHttp.send(parameters);
        }
      };
      
      // returns nothing
      // url: a string uri
      // config: object of(Config)
      this.requestAsync = function(url, config) {
        this.__requestAsyncInternal(url, config, function(xmlHttp) {
          if (xmlHttp.readyState == 4) {
            var data = xmlHttp.responseText;
            config.done(data, config.context);
            
            if (xmlHttp.status == 200) {
              config.success(data, config.context);
            } else {
              config.fail(data, config.context);
            }
          }
        });
      };
      
      // returns nothing
      // url: a string uri
      // config: object of(Config)
      this.requestAsyncJSON = function(url, config) {
        this.__requestAsyncInternal(url, config, function(xmlHttp) {
          if (xmlHttp.readyState == 4) {
            var data = JSON.parse(xmlHttp.responseText);
            config.done(data, config.context);
            
            if (xmlHttp.status == 200) {
              config.success(data, config.context);
            } else {
              config.fail(data, config.context);
            }
          }
        });
      };
      
      
      // returns null
      this.dispose = function() {
      };
      
    });
  }
);

