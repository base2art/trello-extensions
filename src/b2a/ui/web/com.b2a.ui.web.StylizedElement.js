


define("b2a/ui/web/StylizedElement",
      ["b2a/System", "b2a/mxd/Element"],
  function(System, Element) {
    var klazz = System.extend("com.b2a.ui.web.StylizedElement", function(element) {
      this.__nativeEl = element;
      if (element != null && element["unwrap"] != null)
      {
        this.__nativeEl = element.unwrap();
      }
      
      this.superElement(this.__nativeEl);
      
      var EventHandler = function(name, element, parent) {
        this.add = function(func){parent[name] = func;};
        this.remove = function(func){parent[name] = null;};
      };
      
      var EventListener = function(name, element, parent) {
        this.add = function(func) {
          parent.addEventListener(name, func, false);
          return element;
        };
        
        this.remove = function(func) {
          parent.removeEventListener(name, func, false);
          return element;
        };
      };
      
      Object.defineProperty(this, "computedStyle",  
                { get : function(){ return window.getComputedStyle(this.__nativeEl, null); }});
      
      Object.defineProperty(this, "style",  
                { get : function(){ return this.__nativeEl.style; }});
                
      Object.defineProperty(this, "events",  
                { get : function() {
                    var tempNative = this.__nativeEl;
                    return new function() {
                    
                      this.click = new EventHandler("onclick", this, tempNative);
                      this.domNodeRemoved = new EventListener("DOMNodeRemoved", this, tempNative);
                      this.domNodeInserted = new EventListener("DOMNodeInserted", this, tempNative);
                    };
                  }
                });
    });
    
    
    
    klazz.prototype = new Element();
    klazz.prototype.superElement = Element.prototype.constructor;
    klazz.prototype.constructor = klazz;
    
    return klazz;
  }
);
