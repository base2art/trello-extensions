
define("b2a/System",
      [],
  function() {
    var tempSysObj = {
          
      // http://stackoverflow.com/questions/5999998/how-can-i-check-if-a-javascript-variable-is-function-type
      // objToCheck: is obj or null
      isFunc : function (objToCheck) {
        if (objToCheck == null) {
          return false;
        }
        
        var getType = {};
        return objToCheck && getType.toString.call(objToCheck) === '[object Function]';
      },
      
      // parent: <AnyRef>
      // childNamespaces: list
      // funcOrObj: <AnyRef> or <Func>
      extendNative : function (parent, childNamespaces, funcOrObj) {
        if (childNamespaces.length == 0) {
          return;
        }
        
        if (childNamespaces.length == 1)
        {
          if (this.isFunc(funcOrObj)) {
            parent[childNamespaces[0]] = funcOrObj;
            return parent[childNamespaces[0]];
          }
          
          var parentObj = parent[childNamespaces[0]];
          
          if (parentObj != null)
          {
            for(var key in funcOrObj) {
              parentObj[key] = funcOrObj[key];
            }
            
            return parentObj; 
          }
          
          parent[childNamespaces[0]] = funcOrObj;
          return parent[childNamespaces[0]];
        }
        
        var firstName = childNamespaces[0];
        if (parent[firstName] == null)
        {
          parent[firstName] = {};
        }
        
        return this.extendNative(parent[firstName], childNamespaces.slice(1), funcOrObj);
        
      },
      
      // nses: Expect to be list or string
      // funcOrObj: expect to be a function or an object with methods
      extend : function (nses, funcOrObj) {
        if (nses == null) {
          return null;
        }
        
        if (nses["split"]) {
          return this.extendNative(window, nses.split("."), funcOrObj);
        }
        
        if (nses.toString() == nses) {
          return this.extendNative(window, nses, funcOrObj);
        }
        
        return this.extendNative(window, nses, funcOrObj);
      },
      
      // returns null
      // disposableObject: method that implements method dispose
      // func: delegate that acts as a block
      using : function (disposableObject, func) {
        try {
          func(disposableObject);
        }
        catch (err) {
          disposableObject.dispose();
          throw err;
        }
          
        disposableObject.dispose();
      },
      
      // UNTESTED
      //~ extendAsArray : function (obj) {
        //~ if (obj.length === undefined || obj.__lookupGetter__('length')) {
          //~ var index = 0;
          //~ 
          //~ for (var prop in obj) {
            //~ if(!obj.__lookupGetter__(prop)) {
              //~ (function(thisIndex, thisProp) {
                //~ obj.__defineGetter__(thisIndex, function() {return obj[thisProp]});
              //~ })(index, prop);
              //~ index++;
            //~ }
          //~ };
          //~ 
          //~ obj.__defineGetter__("length", function() {return index});
        //~ }
        //~ 
        //~ return obj;
      //~ }
    };
    
    tempSysObj.extend("com.b2a.System", tempSysObj);
    return com.b2a.System;
  }
);
