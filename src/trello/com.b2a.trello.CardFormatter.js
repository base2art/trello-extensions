
define("b2a/trello/CardFormatter",
      [
        "b2a/System",
        "b2a/mxd/Selector",
        "b2a/ui/web/Style"
      ],
  function(System, $, $ui) {
    return System.extend("com.b2a.trello.CardFormatter", {
      
      name: function() { return "card-formatter"; },
      
      apply : function() {
        
        var klass = {
          processCardInternal : function (x) {
            x.events.domNodeInserted.remove(klass.processCard);
            x.events.domNodeRemoved.remove(klass.processCard);
              
            x.class("list-card-title").forEach(function(y) {
              
              y.textChildren.forEach(function(z) {
                
                var parts = z.value.split("|");
                var newText = null;
                var title = null
                var subTitle = null;
                if (parts.length >= 3)
                {
                  newText = parts[0] + " " + parts[2];
                  title = parts[1];
                }
                
                var origin = newText != null ? newText : z.value;
                
                parts = origin.split(/[\[\]]/);
                if (parts.length >= 3)
                {
                  newText = parts[0] + " " + parts[2];
                  subTitle = parts[1];
                }
                
                if (newText != null)
                {
                  // CLEAR ALL ELEMENTS THAT I INSERTED
                  x.class("b2a-heading").forEach(function(y) { y.remove(); });
                  
                  var div = y.addElementBefore('div', {"class": "b2a-heading"});
                  
                  if (title != null)
                  {
                    var headTitle = div
                                      .addElement('h4', {style: "border-bottom: solid 1px;"})
                                      .addText(title)
                                      .parent();
                  }
                  
                  if (subTitle != null)
                  {
                    
                    var headSubTitle = $ui(div
                                            .addElement('h5')
                                            .addText(subTitle)
                                            .parent());
                    headSubTitle.style.color = "brown";
                    headSubTitle.style.fontSize = "75%";
                  }
                  
                  z.value = newText;
                }
              });
            });
            
            
            x.events.domNodeInserted.add(klass.processCard);
            x.events.domNodeRemoved.add(klass.processCard);
          },

          processListInternal : function (x) {
            x.events.domNodeInserted.remove(klass.processList);
            x.events.domNodeRemoved.remove(klass.processList);
          
            $.class("list-card-details").forEach(function(y) { klass.processCardInternal($ui(y)); });
            
            x.events.domNodeInserted.add(klass.processList);
            x.events.domNodeRemoved.add(klass.processList);
          },
          
          processCard : function () {
            klass.processCardInternal($ui(this));
          },
          
          processList : function () {
            klass.processListInternal($ui(this));
          }
        };
        
        
        $.class("list-card-details").forEach(function(x) {
          klass.processCardInternal($ui(x));
        });

        $.class("list-cards").forEach(function(x) {
          klass.processListInternal($ui(x));
        });
      }
    });
  }
);
