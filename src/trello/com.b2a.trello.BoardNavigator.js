
define("b2a/trello/BoardNavigator",
      [
        "b2a/System",
        "b2a/lang/String",
        "b2a/mxd/Selector",
        "b2a/ui/web/Style",
        "b2a/net/WebConnection"
      ],
  function(System, String, $, $ui, WebConnection) {
    
    return System.extend("com.b2a.trello.BoardNavigator", {
      apply : function() {
        
        var appendBoards = function(parent, boards) {
          boards.forEach(function(board) {
            
            var text = String.endsWith(board.url, window.location.pathname) ? "\u25CA " : ""
            parent.addElement("li")
                  .addElement("a", {href: board.url})
                  .addText(text + board.name);
          });
        };
        
        
        //http://stiankarlsen.me/blog/fake-jquery-select-list/
        $ui.appendCssContent(
                'div.fake-dropdown {' +
                '  width: 400px;' +
                '  border-radius: 5px 5px 0 0;' +
                '  display:none;' +
                '  position: absolute;' +
                '  top: 10px;' +
                '  bottom: 10px;' +
                '  left: 50px;' +
                '}' +
                'div.fake-dropdown ul { ' +
                '  overflow-x: hidden;' +
                '  overflow-y: scroll;' +
                '  min-height: 100px;' +
                '  margin-right: 10px;' +
                '}' +
                'div.fake-dropdown .ul-wrapper { ' +
                '  position: absolute;' +
                '  z-index: 100;' +
                '  left: 0;' +
                '  box-shadow: 3px 2px 4px rgba(0, 0, 0, 0.1);' +
                '  background: #fff;' +
                '  border: 1px solid #ccc;' +
                '  width: 90%;' +
                '  margin: 1em 0 0 1em;' +
                '  border-radius: 5px;' +
                '  background-color: #e4e4e4;' +
                '}' +
                'div.fake-dropdown ul:after, ul:before {' +
                '  bottom: 100%;' +
                '  border: solid transparent;' +
                '  content: " ";' +
                '  height: 0;' +
                '  width: 0;' +
                '  position: absolute;' +
                '  pointer-events: none;' +
                '}' +
                'div.fake-dropdown ul:after {' +
                '  border-color: transparent;' +
                '  border-bottom-color: #fff;' +
                '  border-width: 10px;' +
                '  left: 50%;' +
                '  margin-left: -10px;' +
                '}' +
                'div.fake-dropdown ul:before  {' +
                '  border-color: transparent;' +
                '  border-bottom-color: #ccc;' +
                '  border-width: 11px;' +
                '  left: 50%;' +
                '  margin-left: -11px;' +
                '}' +
                'div.fake-dropdown ul li {' +
                '  cursor: pointer;' +
                '  padding: .7em .75em;' +
                '  font-size: .85em;' +
                '  overflow: hidden;' +
                '}' +
                'div.fake-dropdown ul li:hover, ul li.is-active {' +
                '  background: #f1f1f1;' +
                '  cursor: default;' +
                '}' +
                'div.fake-dropdown ul li img {' +
                '  float: right;' +
                '}' +
                'div.fake-dropdown ul li hr {' +
                '  margin: 5px 0px;' +
                '}' +
                'div.fake-dropdown ul li a {' +
                '  text-decoration:none;' +
                '  display:block;' +
                '}');
        
        var orgName = $.class("js-org-name")[0];
        
         var div = orgName.addElementAfter(
              "div", 
              {"class":"board-header-btn quiet ed org-name-dd"})
          .addElement("span", {"class": "icon-sm board-header-btn-icon"})
          .addText("\u25bc")
          .parent()
          .parent();
          
        System.using(new WebConnection(), function (conn){
        
          var config = conn.config();
          
          // Browser manipulates the href prop
          // if you go directly at it.
          var href = orgName.attr("href");
          var li = href.lastIndexOf("/");
          href = href.substring(li);
          
          
          conn.requestAsyncJSON("/1/Members/me", config.withSuccess(function(meData, context) {
            
            var userId = meData.id;
            
            conn.requestAsyncJSON("/1/Organizations" + href + "?boards=open", config.withSuccess(function(data, context) {
              
              var boardData = data;
              var memberBoards = [];
              var nonMemberBoards = [];
              boardData.boards.forEach(function(board){
                var member = board.memberships.some( function(membership) { return membership.idMember == userId; });
                
                (member ? memberBoards : nonMemberBoards).push(board);
              });
              
              var listAreaWrapper = $ui($.class("list-area-wrapper")[0]);
              
              var fakeDD = $ui(listAreaWrapper.addElement(
                                          "div",
                                          {
                                            "class": "fake-dropdown",
                                            "style": "display:none"
                                          }));
              
              ul = $ui(fakeDD
                        .addElement("div", {"class": "ul-wrapper"})
                        .addElement("ul", {"class": "fancy-scrollbar"}));
              
              var actHeight = parseInt(listAreaWrapper.computedStyle.height);
              
              
              ul.style.height = (actHeight - 100) + "px";
              appendBoards(ul, memberBoards);
              
              if (memberBoards.length > 0 && nonMemberBoards.length > 0) {
                ul.addElement("li")
                  .addElement("hr");
              }
              
              appendBoards(ul, nonMemberBoards);
              
              div.unwrap().onclick = function(){
                var style = fakeDD.style;
                var newDisplay = style.display == "none" ? "block" : "none";
                style.display = newDisplay;
                return false;
              };
            }));
          }));
        });
      }
    });
  }
);
