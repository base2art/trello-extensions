
define("b2a/trello/BurnDown",
      [
        "b2a/System",
        "b2a/lang/String",
        "b2a/mxd/Selector",
        "b2a/ui/web/Style",
        "b2a/net/WebConnection",
        "b2a/trello/ui/BurnDownBuilder"
      ],
  function(System, String, $, $ui, WebConnection, BurnDownBuilder) {
    
    return System.extend("com.b2a.trello.BurnDown", {
      
      name: function(){ return "burn-down"; },
      
      apply : function() {
        
        var getListId = function(data, afterIf) {
          
          if (data.listBefore == null) {
            if (data.list == null) {
              return null;
            }
            
            return data.list.id;
          } else {
            return afterIf ? data.listAfter.id : data.listBefore.id;
          }
        }
        
        var getValidLists = function(cards, cardActionMap) {
          var lists = {};
          Object.keys(cardActionMap).forEach(function(key){
            cardActionMap[key].actions.forEach(function(action) {
              //~ if (action.data["list"] != null) {
                //~ lists[action.data.list.id] = action.data.list.name;
              //~ }
              
              if (action.data["listBefore"] != null) {
                lists[action.data.listBefore.id] = action.data.listBefore.name;
              }
              
              if (action.data["listAfter"] != null) {
                lists[action.data.listAfter.id] = action.data.listAfter.name;
              }
            });
          });
          
          return lists;
        };
        
        var drawBurnDown = function(cards, cardActionMap) {
          
          console.log(cardActionMap);
          var lists = getValidLists(cards, cardActionMap);
          
          var endDate = new Date().clearTime().addDays(1);
          var earliestDate = endDate;
          for(var i in cardActionMap) {
            cardActionMap[i].actions.forEach(function(action){
              var actionDate = new Date(action.date).clearTime();
              if (actionDate < earliestDate) {
                earliestDate = actionDate;
              }
            });
          }
          
          var keys = Object.keys(cardActionMap);
          var i=0;
          var dayMap = {}
          
          var dayCounter = new Date(earliestDate);
          while (dayCounter <= endDate) {
            dayMap[dayCounter] = [];
            dayCounter.addDays(1);
          }
          
          for(i = 0; i < keys.length; i++) {
            var cardId = keys[i];
            var card = cards.filter(function(x){return x.id == cardId})[0];
            var obj = cardActionMap[cardId];
            var actions = obj.actions
                .sort(function(x, y) {
                  return new Date(x.date).compareTo(new Date(y.date));
                });
            
            if (actions.length > 0) {
              
              var dayCounter = new Date(earliestDate);
              var currentAction = actions[0];
              var lastAction = null;
              var counter = 0;
              while (dayCounter <= endDate) {
                
                var cardActionListId = lastAction == null
                    ? null
                    : getListId(lastAction.data, false);
                
                // Break if you flow over boundry
                if (dayCounter >= new Date(currentAction.date).clearTime()) {
                  counter += 1;
                  lastAction = currentAction;
                  currentAction = actions[counter];
                  
                  if (currentAction == null) {
                    currentAction = actions[counter - 1];
                    dayCounter.addDays(1);
                    break;
                  }
                  
                  if (currentAction.type == "moveCardToBoard") {
                    lastAction = null;
                  }
                  //dayCounter.addDays(1);
                  continue;
                }
                
                
                if (lastAction == null) {
                  // NOT CREATED YET!
                  dayCounter.addDays(1);
                  continue;
                }
                
                if (lists[cardActionListId] == null) {
                  // NOT ON CURRENT BOARD YET
                  dayCounter.addDays(1);
                  continue;
                }
                
                dayMap[dayCounter].push({
                  list: cardActionListId, 
                  cardName: (cards.filter(function(x) { return x.id == currentAction.data.card.id})[0]).name, 
                  cardId: currentAction.data.card.id});
                
                dayCounter.addDays(1);
              }
              
              while (dayCounter <= endDate) {
                dayMap[dayCounter].push({
                  list: getListId(currentAction.data, true),
                  cardName: (cards.filter(function(x) { return x.id == currentAction.data.card.id})[0]).name,
                  cardId: currentAction.data.card.id
                });
                dayCounter.addDays(1);
              }
            } else {
              
              var dayCounter = new Date(earliestDate);
              var counter = 0;
              while (dayCounter <= endDate) {
                // ADD IN CURRENT List
                dayMap[dayCounter].push({
                  list:     card.idList, 
                  cardName: (cards.filter(function(x) { return x.id == card.id})[0]).name, 
                  cardId:   card.id
                });
                dayCounter.addDays(1);
              }
            }
          }
          
          var dayCounter = new Date(earliestDate);
          while (dayCounter <= endDate) {
            if (dayMap[dayCounter].length == 0) {
              delete dayMap[dayCounter];
            }
            else {
              break;
            }
            dayCounter.addDays(1);
          }
          
          console.log(dayMap);
          var builder = new BurnDownBuilder(dayMap, lists);
          builder.render();
        };
        
        var orgName = $.class("js-org-name")[0];
        
        var div = $ui(orgName.parent()
              .addElement("div", {
                      "class":"board-header-btn quiet ed",
                      "style": "padding: 0 8px;"
                    })
              .addText("Burn Down")
              .parent());
            
            //.addElement("span", {"class": "icon-sm board-header-btn-icon"})
        
        div.events.click.add(function(){
        
          System.using(new WebConnection(), function (conn){
          
            var config = conn.config();
            
            // Browser manipulates the href prop
            // if you go directly at it.
            var boardId = String.split(window.location.pathname, '/')[1];
            
            
            conn.requestAsyncJSON("/1/board/" + boardId + "/cards", config.withSuccess(function(cards, context) {
              var map = {};
              
              cards.forEach(function(card){
                conn.requestAsyncJSON("/1/card/" + card.id + "/actions?filter=updateCard:idList,createCard,moveCardToBoard", config.withSuccess(function(actions, context) {
                  
                  map[card.id] = { "card": card, "actions": actions };
                  if (Object.keys(map).length == cards.length) {
                    drawBurnDown(cards, map);
                  }
                }));
              });
            }));
          });
        });
      }
    });
  }
);
