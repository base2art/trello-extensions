
define("b2a/trello/contentScript",
      [
        "b2a/ui/web/Style",
        "b2a/trello/CardFormatter",
        //"b2a/trello/BoardNavigator",
        "b2a/trello/BurnDown"
      ],
  function($ui) {
    
    var applyPlugins = function(plugins, count, max, debug) {
      
      if (count > max) {
        return ;
      }
      
      
      var path = window.location.pathname;
      console.log("Page Change Event to: " + path);
      
      if (path.length > 3 && path.substring(0, 3) == "/b/") {
        var i = 0;
        for (i=0; i < plugins.length; i++) {
          if (debug) {
            plugins[i].apply();
          } else {
            try { 
              plugins[i].apply(); 
            } catch(err) {
              console.log("Plugin failed to execute");
              console.log(err);
              setTimeout(function(){applyPlugins(plugins, count + 1, max, debug)}, 2000);
            }
          }
        }
      }
    };
    
    $ui.appendJavaScriptContent(
      'Backbone.history.bind("all", function(route, router) {' +
      '  window.postMessage({ type: "BACKBONE_CHANGE_PAGE", text: "" }, "*");' +
      '});'
    );
    
    var isDebug = false;
    
    
    var plugins = Array.prototype.slice.call(arguments, 1)
    chrome.storage.sync.get(function(items) {
      
      plugins = plugins.filter(function(plugin){
        
        var disabled = items[plugin.name()];
        if (disabled == null) {
          disabled = false;
        }
        
        return !disabled;
      });
      
      applyPlugins(plugins, 0, 1, isDebug);
      
      setTimeout(function(){
        // https://developer.chrome.com/extensions/content_scripts.html#host-page-communication
        window.addEventListener("message", function(event) {
          if (event.source != window) {
            return;
          }
          
          if (event.data.type && (event.data.type == "BACKBONE_CHANGE_PAGE")) {
            setTimeout(function(){applyPlugins(plugins, 0, 1, isDebug);}, 1000);
          }
        }, false);
      }, 1000);

    });
  }
);
