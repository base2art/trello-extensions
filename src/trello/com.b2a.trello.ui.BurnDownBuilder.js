
define("b2a/trello/ui/BurnDownBuilder",
      [
        "b2a/System",
        "b2a/lang/String",
        "b2a/mxd/Selector",
        "b2a/ui/web/Style"
      ],
  function(System, String, $, $ui) {
    
    return System.extend("com.b2a.trello.ui.BurnDownBuilder", function(dateMap, lists) {
      this.__dateMap = dateMap;
      this.__lists = lists;
      
      this.showOverlay = function() {
        
        //var settingsFrameId = 'settingsFrame';
        $.doc().tag('body')[0].addClass("window-up");
        var windowObj = $ui($.doc().class('window')[0]);
        windowObj.style.display = "block";
        windowObj.style.top = "50px";
      };
      
      this.hideOverlay = function() {
        $.doc().tag('body')[0].removeClass("window-up");
        var windowObj = $ui($.doc().class('window')[0]);
        windowObj.style.display = "none";
        
        windowObj.style.width = "495px";
        windowObj.style.left = "195px";
        
        //~ $(window).unbind('resize', repositionBurndown);
        //~ $('.window-header-utils a.js-close-window').unbind('click', hideBurndown);
        //~ $('.window-wrapper').unbind('click', ignoreClicks);
        //~ $('.window-overlay').unbind('click', hideBurndown);
      };
      
      this.render = function() {
        
        var modalWindow = $ui($.doc().class('window')[0]);
        modalWindow.style.width = "100%";
        //~ modalWindow.style.left = "195px";
        modalWindow.style.left = "0px";
        
        var wrapper = $ui(modalWindow.class('window-wrapper')[0]);
        wrapper.elementChildren.forEach(function(x) { x.remove(); });
        
        var anchor = wrapper
               .addElement("div", { class: "dialog-close-button" })
               .addElement("a", {
                  class: "icon-lg icon-close dark-hover js-close-window",
                  href:  "#",
                  title: "Close this dialog window."
                });
        $ui(anchor).events.click.add(this.hideOverlay);
        
        var wrapperChild = wrapper.addElement("div", { class: "clearfix editable" });
        
        var anchor = wrapperChild
               .addElement("div", { class: "window-header clearfix" })
               .addElement("span", { class: "window-header-icon icon-lg icon-card" })
               .addElementAfter("div", { class: "window-title non-empty inline"})
               .addElement("h2", { class: "window-title-text current" })
               .addText("Burn Down");
        
        var keyMap = {};
        Object.keys(this.__dateMap).forEach(function(x) { keyMap[new Date(x).toString("ddd - MM/dd")] = x; });
        
        
        var sortedKeys = Object.keys(this.__dateMap)
          .sort(function(x,y) { return new Date(x).compareTo(new Date(y)); })
          .map(function(x) { return new Date(x).toString("ddd - MM/dd"); });
        
        var modMatcher = Math.floor(sortedKeys.length / 20.0) + 1;
        
        sortedKeys = sortedKeys.filter(function(x, y ){ return y % modMatcher == 0; });
        
        var multiplier = 60;
        if (sortedKeys.length > 10) {
          mupltiplier = 50;
        }
        
        if (sortedKeys.length > 15) {
          mupltiplier = 40;
        }
        
        var canvas = wrapperChild
               .addElement("div", { "class": "window-main"})
               .addElement("div", { "class": "window-main-col-content clearfix"})
               .addElement("canvas", {id:"myChart", width: (sortedKeys.length * multiplier) + "", height:"400"});
        if (sortedKeys.length > 14) {
          modalWindow.style.left = "20px;";
        }
        
        var ctx = canvas.unwrap().getContext("2d");
        
        var listKeys = Object.keys(this.__lists);
        var colors = $ui.generateColors(listKeys.length);
        var ds = [];
        //console.log(this.__lists);
        
        var datumMap = {};
        listKeys.forEach(function(listId) {
          datumMap[listId] = [];
          sortedKeys.forEach(function(dateStr) {
            datumMap[listId].push(0);
          });
        });
        
        var cardDataPerDay = this.__dateMap;
        listKeys.forEach(function(listId) {
          var color = colors[ds.length];
          var datumCounter = 0;
          
          sortedKeys.forEach(function(dateStr) {
            var dateKey = keyMap[dateStr];
            var currentDateData = cardDataPerDay[dateKey];
            datum = datumMap[listId];
            currentDateData.forEach(function(card) {
              if (card.list == listId) {
                var pointFinder = /\((\d(\.\d)?)\)/;
                var matches = pointFinder.exec(card.cardName);
                var points = 1;
                if (matches != null && matches.length > 1) {
                  points = parseFloat(matches[1]);
                }
                
                if (isNaN(points)) {
                  points = 1;
                }
                
                datum[datumCounter] = datum[datumCounter] + points;
              }
            });
            
            datumCounter += 1;
          });
          
          listValues = datum.map(function(){ return lists[listId]; });
          ds.push(
          {
            data: datum,
            fillColor : "rgba(" + color[0] + ", " + color[1] + ", " + color[2] + ", 0.6)",
            labels: listValues
            
          });
        });
        
        var data = {
          labels : sortedKeys,
          datasets : ds
        }
        
        var myNewChart = new Chart(ctx).Bar(data, {stacked: true});
        
        /*
        <div class="window-main-col">
          <div class="card-detail-metadata clearfix gutter">

          </div>
          <div class="window-main-col-content clearfix">
          </div>
        </div>

        */
        
        //var windowHeaderUtils = $('<div/>', {class: 'window-header-utils dialog-close-button'}).append( $('<a/>', {class: 'icon-lg icon-close dark-hover js-close-window', href: '#', title:'Close this dialog window.'}) );
        
        this.showOverlay();

        // Build the dialog DOM elements. There are no unescaped user-provided strings being used here.
        //var clearfix = $('<div/>', {class: 'clearfix'});
        //var settingsIcon = $('<img/>', {style: 'position:absolute; margin-left: 20px; margin-top:15px;', src:scrumLogo18Url});

      };
    });
  }
);
      
