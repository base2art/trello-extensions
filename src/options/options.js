

var plugin_keys = ["card-formatter", "burn-down"];

function setValueToStorage(items, key) {
	var element = document.getElementById(key);
  items[key] = !element.checked;
}

function getValueFromStorage(items, key) {
	var element = document.getElementById(key);
  element.checked = (items == null) || (items[key] == null) || items[key] !== true;
}

function eraseOptions() {
	chrome.storage.sync.remove(plugin_keys);
	location.reload();
}

function saveOptions() {
  
  var items = {};
  plugin_keys.forEach(function(key){ setValueToStorage(items, key);});
	chrome.storage.sync.set(items);
}

function loadOptions() {
	chrome.storage.sync.get(plugin_keys, function(items) {
    
    plugin_keys.forEach(function(key){ getValueFromStorage(items, key);});
  });
  
  document.getElementById("saveButton").onclick = saveOptions;
  document.getElementById("eraseButton").onclick = eraseOptions;
}

window.onload = function(){ loadOptions() };

